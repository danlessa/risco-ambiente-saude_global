# Reflexões compiladas – aula 2

# Reflexão de Iara

A relação sociedade e ambiente se descrevem pelas categorias que assumem ao longo do tempo e espaço, se num primeiro momento ambas eram determinadas por infligir sempre a categoria outro, agora as fronteiras deixaram de existir – as relações de dependência entre ambos (sociedade e ambiente) foram interrogadas a partir do perigo (silencioso ou silenciado) anunciado. As fronteiras não estão claras e tão pouco, estão certas as proporções desastrosas que podem advir dos riscos ‘modernos’. A tendência de globalização ocupa os níveis dos riscos ambientais e tecnológicos – e não encontra fronteiras: econômicas, políticas e/ou sociais, tendo em vista que sua gravidade quase nunca é perceptível fisicamente, todavia é globalmente alcançável e muitas vezes, diz-se de danos irreversíveis – o que torna a avaliação desses problemas complexo, pois as medidas de análise não podem se pautar apenas em relações numéricas e unívoca. 

A desconcertante relação posta à tríade: ambiente, sociedade e desastres só aumentam e a cada evento as relações tornam-se mais conflitivas politicamente. A pretensão de racionalidade ao determinar objetivamente o risco dos riscos ainda enfrenta limitações por motivos políticos e econômicos, geralmente; muito não é declarado e estas omissões quase sempre são drásticas. Questões globais não podem ser encaradas sob olhares locais apenas, principalmente quanto fatores econômicos estão evidentes. É preciso realocar a própria noção de risco e suas dimensões; ainda que se fale em risco global, as diferenças entre as nações colocam algumas em contextos mais vulneráveis, tanto no nível dos efeitos dos desastres, quanto das contas a pagar pela garantia do seu direito de se desenvolver. São os países pobres os mais suscetível aos riscos, devido a baixa tecnologia e baixas penalidades enfretadas pelos grades produtores de risco em caso de desastres.

A globalização, como já comentado alinha-se tanto em aspecto econômico, quanto político, cultural e informacional e para além dos aspectos favoráveis, traz também novas problemáticas em nível de saúde. Para além das relações com os desastres ambientais e tecnológicos comentados, traz também a dimensão da interdependência envolvendo todos os aspectos humanos e sociais: relações de trabalho, segurança sanitária, consumo de bens, produtos e alimentos.  A complexidade do tema exige que: se conheça a distribuição desigual das doenças e dos agravos à saúde ao redor do mundo, se estude os impactos das mudanças ambientais globais na saúde humana e formas de mitigação e adaptação e por fim se compreenda as relações entre políticas, instituições e sistemas de Saúde Global. 

Nessa relação global pensando conjunto: social e ambiental, colocando tantas questões em voga, tem ainda outra problemática que quase sempre assume o mais delicado debate: responsabilidade diante das ocorrências e o que pode ser considerável aceitável ou não diante do desenvolvimento geral; quem assume a responsabilidade por essas medições, quem assume o papel de árbitro nestas causas. O fato é que se torna necessário um contingente de leis, seguros e política neutras, que não fabriquem falsos resultados, nem problemas. Sugere-se para isso o trabalho com o princípio da precaução, todavia, cada vez mais ele se torna inviável para medição, por tudo que já foi comentado: pela perda da fronteira dos desastres que tem ocorrido, por exemplo. A noção de risco precisa ser reavaliada assim como a responsabilidade, o preço das incertezas precisa ser direcionado. Lembrando que o risco está intimamente associado com responsabilidade, mas também com a iniciativa e a exploração de novos horizontes, trata-se de processo de tomada de decisões – mesmo que envolva decidir por avaliar um risco a nível abstrato e isso não supõe que venha a se concretizar, essa é na verdade a opções que não se quer, mas é preciso considerá-lo pois o risco liga-se à possível autodestruição da vida na terra, precisamos nos reconhecer enquanto – sujeito e objeto das relações que a partir de então estão sendo estabelecidas entre sujeitos e ambiente.

## Reflexão de Nilton

Em uma visão sociológica, podemos entender os riscos como incertezas fabricadas  e ameaça de efeitos colaterais que surgem com a modernização das atividades realizadas pelas instituições humanas.  É perceptível que grande desafio para a saúde global continua sendo a lógica do Capitalismo que visa ao lucro para uma pequena parte da população e em escala cada vez maior. Interessante pensar que isso ocorre na era da sociedade de risco em que as consequências das decisões tomadas não estão mais limitadas ao local de ocorrência e, muito menos, limitadas ao espaço de tempo em que ocorrem.

A desigualdade fica evidente ao pensar que os efeitos colaterais, gerados pelas atitudes de algumas organizações, atingem primeiro e de forma muito mais grave as populações que vivem em condições vulneráveis. Fato que ainda é agravado pela crise de responsabilidade, uma vez que governo, mercado, indústria ou outras instituições não assumem a culpa e obrigações de suas decisões, pois isso implicaria em redução de lucro. Assim, nota-se um desmonte de políticas e de meios que garantam um mínimo de segurança para o ambiente e para a população.

A exemplo disso, podemos pensar no consumo alimentar via plataformas digitais como Ifood, Rappi ou Uber. A falta de regulamentação desse tipo de serviço acarreta em problemas que envolve tanto a flexibilização das relações de trabalho e efeitos nocivos a saúde do trabalhador em situações precárias, quanto a saúde dos indivíduos que irão consumir os alimentos que normalmente são os fast foods. Fica uma impressão de que "vale tudo" na luta entre o lucro e saúde pública.

## Reflexão de Vanessa

No passado a relação com os danos causados era limitada somente ao “outro”, ou seja,  somente os outros sofreriam as consequências de atos errôneos, entretanto a partir das guerras mundiais e acidentes como o de Chernobyl, começou a se entender que todos são atingidos e todos são prejudicados, sendo reforçado pela percepção de Beck no trecho “é preciso que um pouco mais do futuro se torne visível” pois dessa forma é possível que saibamos lidar melhor com o mesmo.

“Podem (grupos de) países ser mantidos em quarentena?” o que observamos diante da pandemia do COVID 19 (corona vírus) é exatamente um resposta para tal questionamento, ou ao menos a tentativa, uma vez que observamos a China e a Itália estabelecendo quarentenas para seus habitantes, com a finalidade de evitar mais mortes.

Risco e responsabilidade são estritamente ligados, é utilizado a inovação cientifica para exemplificar quando o Giddens cita o consumo do vinho ou carne como benéficos ou maléficos à saúde humana, ele cita Beck sobre a percepção de “irresponsabilidade organizada”, fazendo o questionamento “Quem deve determinar os produtos nocivos quais são os efeitos colaterais produzidos por eles e qual o nível de risco aceitável?” e cita o “princípio da precaução” como limitante para responsabilização.

É preciso analisar o risco a partir dos fatores positivos e negativos, o que é justificado por Giddens quando cita a questão dos riscos ambientais, onde a maioria são analisados isoladamente, não ponderando questões sociais e econômicas relacionadas, entretanto quando ocorrem desastres ambientais como o de Brumadinho e Mariana todos os aspectos (social, ambiental e econômico) são prejudicados e passam a ser analisados.
“processos de interdependência planetária trazem benefícios e riscos para a saúde humana”, quando analisamos a globalização, se percebe que há uma relação entre o que acontece no planeta a todos que residem nele, entretanto o que muda é a intensidade aos atingidos, ou seja nem todos são atingidos e nem da mesma forma, o que é evidenciado através da análise de que a globalização pode resultar em riscos à saúde e fatores como a questão econômica (pobreza) potencializam os efeitos e o número de atingidos.

## Reflexão de Fernando

**Beck**: Vivenciamos a realidade não cenográfica da era pós-burguesia. Ou em relação aos riscos civilizacionais: somos os herdeiros de uma crítica da cultura tornada real, que justamente por isso já não se pode dar por satisfeita com o diagnóstico da crítica da cultura, que em todo o caso sempre foi pensado como uma advertência pessimista quanto ao futuro,  Não é possível que uma Era inteira escorregue para um espaço situado além das categorias atais, sem que esse além seja antes percebido e demarcado como aquilo que é: “ Uma pretensão ordenadora do passado que se prolonga para além de si mesma e da qual tanto presente como futuro se desprenderam”. 
Como em um filme: “De volta para o Futuro”.

A pretensão de racionalidade das ciências de determinar objetivamente o teor de risco refuta-se a si mesma permanentemente: ela baseia-se, por um lado, num castelo de cartas de conjecturas especulativas e move-se unicamente no quadro de asserções de probabilidades, cujos prognósticos de segurança não podem, a bem da verdade, ser refutados se quer por acidentes reais. (Pag. 35)
Asserções: afirmação categórica; assertiva, asserto.

**GIDENS**: A ideia de "Sociedade de Risco" pode sugerir um mundo que se tornou mais perigoso, mas isso não é necessariamente verdade. Pelo contrário, é uma sociedade cada vez mais preocupada com o futuro (e também com a segurança), que gera a noção de risco. (Pag. 3)
O risco está sempre relacionado à segurança e proteção. Também está sempre conectado a responsabilidade. Não é de surpreender, portanto, que à medida que avançamos em direção a um mundo dominada pela incerteza fabricada, e não pela externa, existe uma renovada discussão sobre a natureza da responsabilidade. (Pag. 7)

Lidar com situações de “irresponsabilidade organizada” provavelmente se tornará cada vez mais importante nos campos do direito, seguros e política, mas isso não será fácil de fazer, precisamente devido ao caráter bastante imponderável da maioria circunstâncias de risco fabricado. O dilema do medo contra encobrimentos é uma indicação direta da natureza profunda de problemas envolvidos aqui. (Pag. 8)

**FORTES**: A globalização econômica, que está aliada a globalização política, cultural, informacional e comunicativa, traz novas oportunidades e desafios, cujos benefícios e impactos adversos, que envolvem todas as dimensões das relações humanas, ainda carecem de análises compreensivas.
Processos de interdependência planetária trazem benefícios e riscos para a saúde humana.

## Reflexão de Gustavo

Os Texto Risk and Responsability, de Anthony Giddens e Sociedade de Risco de Ulrich Beck tem em comum além do tema, a característica de terem sido escritos há mais de 20 anos e serem bastante atuais. O livro de Beck é de 1986 e ao ler parece ter sido especialmente para o momento que vivemos. 

O texto de Giddens baseia-se em grande medida no de Back e, portanto, iniciarei os comentários falando de sociedade de Risco, de Beck. 
O objetivo destas reflexões é tentar compilar o que mais me chamou atenção dos textos.

O Autor cita como uma característica de nossa sociedade moderna o fascínio pela produção de riscos e de riqueza, com a diferenciação de que na lógica da fábrica/na sociedade industrial, a produção de riscos podia ser contida ao ambiente em que a produção estava inserida e na produção de riscos da nossa sociedade hoje, “eles não podem ser limitados geograficamente ou em função de grupos específicos”, o que faz surgir ameaças globais supranacionais.

Eu citaria o covid-19 e ameaças aos sistemas democráticos ocorrendo ao redor do mundo, em países com culturas tão distintas, como riscos da sociedade moderna e interconectada, além das flexibilizações da jornada e do local de trabalho como algo que dilui as fronteiras entre o trabalho e o ócio, citada pelo autor.

O autor traz a questão do compartilhamento de riscos como algo que atinge a todos, e alega que no sistema atual, mesmo os responsáveis por produzir os riscos e ganhar com eles, mais cedo ou mais tarde serão atingidos. Porém, a capacidade de lidar com situações de risco, ou compensá-las, são desigualmente distribuídas entre as distintas camadas de renda e educação. Segundo Back, existe uma “força de atração” entre pobreza extrema e riscos extremos. 

A lógica da escassez impera como algo e dita, em especial no terceiro mundo, a transição da sociedade de classes, para a sociedade estratificada até a sociedade individualizada. 

O autor traz também os riscos da sociedade atual como sendo muitas vezes imperceptíveis sensorialmente, como toxina nos alimentos e ou ameaça nuclear. 

Ele caracteriza nossa Sociedade de Risco como uma sociedade catastrófica, na qual o estado de exceção ameaça converter-se em normalidade. 
Um tema que Beck traz algumas vezes ao longo do texto é a produção de alimentos, liberação de agrotóxicos para grandes empresas e culpabilização indevida dos agricultores pelo uso, que o fazem muitas vezes por não encontrar alternativas economicamente viáveis para a produção e ficarem reféns das empresas que produzem os agrotóxicos.

O risco baseia-se num componente futuro e previsível de perda geral de confiança, em questões iminentes e que teoricamente podem ser projetadas, mas que algumas vezes acabam implicando em algo irreal. A sociedade do risco vive no futuro, mais do que no passado ou no presente. E o avanço da sociedade de risco que marca aqueles afetados pelos riscos e aqueles que lucram com eles.

Giddens complementa a visão sobre risco trazendo alguns conceitos complementares do aumento da produção e mercantilização de riscos também pelo fato de estarmos cada vez mais sob influência da ciência e da tecnologia.  Ele caracteriza a sociedade de risco como a alta fronteira tecnológica na qual ninguém entende completamente os riscos assumidos e os futuros possíveis.

Ele fala de “fim da natureza”, afirmando que restam poucos, se algum aspecto da natureza não tocado pelo homem. E que em algum ponto nos últimos 50 anos o que nós estamos fazendo com a nutureza e não o que a natureza pode fazer conosco (pragas, furacões, secas, etc) passou a ser uma questão relevante. Essa transição marca nossa entrada na sociedade de risco.

Além do conceito acima, ele fala do fim das tradições, a vida não é vivida como um destino, que caracteriza também o processo que Beck chama de individualização.

O risco e a percepção de risco são questões que Giddens explora em seu texto, exemplificando que na Idade Média, viver era mais perigoso que nos dias atuais, mas não havia esta percepção pois o perigo estava dado como algo intrínseco à vida. E que atualmente risco está associado à ideia de controlar o futuro, a uma sociedade que tem esta preocupação como algo importante e que deseja segurança.

Em termos de decisões de comportamento individuais, de grupos ou da sociedade, existe um “trade off” que premia ou pune quem se arrisca. Em geral, quanto maior o risco, maior reconhecimento e recompensas serão oferecidas.

O risco externo é representado pelo risco que ocorre com uma população grande de indivíduos e que é previsível e portanto, passível de ser segurado.

O risco manufaturado – é o risco criado pela progressão da sociedade humana, e para o qual a história pouco nos pode fornecer de insumo e material para entendê-lo.

A sociedade de risco apresenta uma característica de que riscos devem ser claramente publicizados para convencerem que o risco é real porém governos e “gerenciadores” devem estar atentos para não serem alarmantes demais ao enunciarem estes riscos. 

O autor diz que a sociedade de riscos vista de maneira positiva é também uma sociedade com maior poder de escolha, obviamente impactada por classe e renda.

O terceiro texto fala sobre Saúde Global, de Fortes e Ribeiro, trata do tema Saúde Global, da evolução do conceito de saúde internacional para saúde global e de como os impactos da globalização afetam em enorme medida o que se entende por saúde e doença nos dias atuais.

Como pontos chaves deste texto eu destacaria o que o autor e a autora denominam de interdependência planetária, que este processo traz benefícios e riscos para a saúde humana, de forma diferenciada ao redor do globo. Inevitável não citar mais uma vez o covid-19, o crescente aumento de casos, as providências globais mostram respostas rápidas e acertadas ou fragilidades advindas da interdependência citada? O covid-19 é considerado uma emergência de Saúde Pública de Importância Internacional (ESPII) pela OMS.

A pressão por respostas e ação insere uma pressão positiva nos países e órgãos multilaterais ou geram caos e excessos? E mais, o surgimento do covid-19 é advindo das questões de interdependência planetária? Temos estas respostas agora ou teremos apenas em algum tempo, após análises mais profundas? Ribeiro e Fortes esclarecem que para um problema de Saúde ser considerado global, ele carrega a necessidade de estabelecimento de acordos e regulamentação internacional para a sua solução, que é certamente o que vem ocorrendo.

Não há um consenso sobre o que seja saúde global e nem uma definição única. Mas certamente ela passa por considerar aspectos humanos, geográficos, de relações internacionais, ambientais e de saúde pública englobadas neste conceito.

## Reflexão de Ana Luisa

Os autores colocam que “os impactos sociais, culturais e econômicos resultantes da globalização podem redundar em riscos à saúde” devido a determinados aspectos. Pensando na lógica capitalista e em como a globalização possibilita a implantação da mesma, me chamam a atenção dois aspectos citados: aumento da obesidade e aumento do consumo de alimentos, cuja produção ou processamento favorecem dietas não saudáveis".

É possível notar que cada vez mais esse modelo de consumo capitalista se apropria de elementos culturais importantes como a comida, estilo de alimentação, etc. Vemos que, por exemplo, os alimentos processados estão cada vez mais presentes na alimentação de países onde antes predominavam alimentos mais naturais. A maneira de consumir esses alimentos processados e a frequencia, muitas vezes relacionados a um consumismo “importado” (como visto no texto: “a adoção por parte de países em desenvolvimento de padrões alimentares não saudáveis importados de países ricos”), vem sendo espalhadas via globalização, afetando diretamente o aumento da obesidade (sem contar, claro, outros fatores como sedentarismo, etc.)

Particularmente eu não conhecia o conceito de Saúde Global. Dentro da Geografia nao tive nenhuma disciplina que contemplasse o tema, que tem tudo a ver com a abordagem geográfica. Ideias como as de “acesso equitativo à saúde em todas as regiões do mundo”, “ênfase nas pessoas, considerando-se a diversidade humana, cultural e social”, “questões e problemas de saúde supraterritoriais que extrapolam as fronteiras geográficas nacionais”, a questão do poder, poderiam ser debatidas no contexto de disciplinas como Geopolítica, Territorialização Mundial, Meio Ambiente Urbano, entre outras e agregariam muito para a área da Geografia.

Como um exemplo que abrange várias questões abordadas no texto, pensei na problemática de como a AIDS é tratada (no sentido de como governos lidam com a doença) no mundo de diferentes formas e como essas diferenças estão atreladas algumas vezes ao desenvolvimento econômico do país, outras a corrente política atual dos governos, entre outros vários fatores.

## Reflexão de Ione

Tendo por referência a discussão do conceito de sociedade de risco trazida pelos textos indicados, parte dos riscos decorrentes e fabricados pelo desenvolvimento da sociedade industrial são percebidos por quem é diretamente afetado pelos efeitos concretos e diretos. Porém, riscos discutidos no passado ou no presente podem não se concretizar ou se concretizar de forma indireta, não sendo muitas vezes visíveis, de difícil identificação de suas origens, uma vez que os efeitos da “modernização” industrial e tecnológica provocam mudanças no ambiente em longas cadeias de produção que atravessam fronteiras territoriais.

Muitos dos riscos indicados ou discutidos no passado já se concretizaram para parte da população, aquela que tem menos recursos e que não tem acesso financeiro a proteções disponíveis. Por outro lado, temos parte da população com recursos para proteção e por esse motivo pouco impactada pelas mudanças e riscos concretizados que, mesmo percebendo que os efeitos da “modernização” industrial e tecnológica existem, tendem a não buscar ações efetivas. Os efeitos negativos provocados tanto no ambiente como na saúde - que em meu entendimento estão interligados - ficam concentrados nas áreas de pobreza extrema onde a preocupação maior está na escassez material e não nos riscos provocados pelos avanços tecnológicos.

Além das diferenças de impacto na população, temos também interesses econômicos e políticos que dificultam a discussão e ações efetivas. Em função de interesses financeiros, os grupos se empenham em criar discursos e paliativos para negar ou postergar os efeitos negativos já ocorridos ou buscar medidas de precaução para eventos negativos que podem ocorrer no futuro.

Como os efeitos da sociedade de risco atravessam fronteiras geográficas, as ações e acordos globais são fundamentais, sem deixar de lado possibilidades de discussões e empenho locais que podem ser decorrentes ou não dos acordos globais.

## Reflexão de Danilo 

**O risco como novo paradigma estruturante da sociedade globalizada**

No começo do século 21, difunde-se crescentemente a noção de esgotamento dos paradigmas associados a ideía comum da história como sendo uma linearidade do desenvolvimento e da evolução, ambos dos quais são pautados por uma lógica técnico-científica que se apresenta como cada vez mais onipotente perante a natureza como um todo. Este é o modernismo, e o seu desenrolar, ao atingir seus limites e obrigar para uma reflexidade de si próprio, dá luz ao precursor de uma sociedade pós-moderna: aquela no qual a incerteza futura acoplada para com a superação em massa das necessidades básicas acaba por tornar o risco como item central da organização social.
Nesta nova sociedade, nem moderna no sentido tradicional e nem pós-moderna, a divisão deixa de ser entre aqueles que tem ou nao tem - a luta de classes, mas sim o é por meio entre aqueles que estão em condições de alto risco e baixo risco. Não mais a disputa o é pela posse da materialidade, mas sim pelo escapamento do risco percebido. A necessidade é substituida pela ansiedade no que tange a nova motivação de ação coletiva.

Tais afirmativas decorrem dos resultados impostos pela modernidade, com o advento de disrupções rotineiras que apresentam consequências imponderáveis e imprevisíveis sobre o modo de vida bem como a saúde e condição geral das pessoas, assim como a sua abrangência geoespacial. Para essencialmente todas as frutos modernos, não há histórico e nem precedente sobre os efeitos de sua geração, uso e descarte, e nem tradição de como lidar com estes.

Para toda essa incerteza, acoplada da ansiedade, gera-se o risco, um elemento que transcende territórios e classes sociais. A estrutura social passa a se dividir e organizar em torno de consumidores e geradores de risco, o qual é sempre percebido somente parcialmente mediante a posse e/ou fabricação de informação.

Em relação a materialidade, o risco diferencia-se por ser percebido ao invés de se ter posse. Não há uma divisão rígida entre os que tem e não tem, mas sim um espectro entre extremo risco e baixo risco. Adicionalmente, o risco é inclusivo ao invés de excludente: todos estão sujeitos a ele em diferentes graus, mesmo que em classes e territorialidades distintas.

Este último aspecto leva para a relevância do entendimento do risco em uma escala global: a conectividade bem como escala dos efeitos oriundos das tecnológias modernas fazem com que o risco seja globalizado em natureza. Isso é evidente especialmente ao analisar as interações entre as nações ditas como subdesenvolvidas, no qual a necessidade prevalece, e as ditas como desenvolvidas, no qual o risco prevalece. Especificamente, o risco somente é percebido após a superação do modernismo enquanto alicercado na necessidade. Isso faz com que sociedades subdesenvolvidas ignorem o risco dos processos modernizantes, tornando-se geradoras de riscos.

Finalmente, há de se considerar que a conotação do risco pode ser negativa ou positiva a depender da perspectiva de observação e transformação. Em contextos empresariais e disruptivos, a mesma adquire dimensão positiva em diversas instâncias. O mesmo não pode ser dito quando tratativo de contextos tradicionais. A percepção do risco, por sua natureza, tem permeabilidade distinta que pode ser influênciada por diversos fatores, da conectividade e acesso à informação para o contexto economico-social.