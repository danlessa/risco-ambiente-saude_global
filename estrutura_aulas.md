Disciplina Risco, ambiente e saúde global SAS 5709 
Responsável: Profa. Dra. Gabriela Marques Di Giulio
Contato: ggiulio@usp.br
Horário: Terças-feiras, das 14 às 18 horas
Sala: Walter Belda

Este curso se propõe a discutir elementos que compõem o debate atual sobre as dimensões sociais do risco, privilegiando diferentes perspectivas teóricas e metodológicas das Ciências Sociais, em particular da Sociologia Ambiental, da Sociologia do Risco e Incerteza e da Comunicação Social, que enfocam as interpretações e vivências que os grupos sociais atribuem às experiências de risco. Por meio de referências teóricas e instrumentos de análise sobre as dimensões sociais do risco, a disciplina busca ampliar o interesse investigativo sobre modos de vida e dinâmicas contemporâneas na formação dos pós-graduandos. 

## Estrutura das aulas – Programa 2020

### Semana 1 – Terça, 03 de março

**Introdução - Risco, ambiente e saúde global: afinal do que estamos falando? Que ideia temos sobre esses conceitos? Como nossas pesquisas dialogam com estes conceitos?**

*Apresentação da docente e da turma*


### Semana 2 – Terça, 10 de março

**A relação entre sociedade e ambiente. Riscos: incertezas fabricadas? O que se coloca como desafio à interface risco, ambiente e saúde global?**

#### Leitura obrigatória

* Beck, U. Nascimento, Sebastião (trad). Sociedade de risco rumo a uma outra modernidade.   2. ed..  São Paulo, Ed. 34, 2011. 383 p (prefácio e capítulo 1)
* Giddens, A. Risk and Responsibility. The Modern Law Review Limited 1999 (MLR 62:1, January).
* Fortes, P.A.C.; Ribeiro, H. Saúde Global em tempos de globalização. Saude soc. [online]. 2014, vol.23, n.2 [cited  2015-02-23], pp. 366-375.


#### Leitura complementar

* Bosco, E. Ulrich Beck: considerações sobre sua contribuição para os estudos em ambiente e sociedade e desafios. Ambiente & Sociedade, n. 2 n p. 149-160 n abr.-jun. 2015.
* Hannigan, J.A. 2006. Environmental sociology – a social construction perspective. Routledge, London (capítulo 8)
* Lieber, R.R. e Romano-Lieber, N.S. 2002. O conceito de risco: Janus reinventado. In: Minayo, M.C.S. e Miranda, A.C. (orgs). Saúde e ambiente: estreitando nós. Fiocruz, Rio de Janeiro, p. 69-111. 
* Guivant, J.S. 1998. A trajetória das análises de risco: da periferia ao centro da teoria social. Revista Brasileira de Informações Bibliográficas, Anpocs, 46: 3-38.

### Semana 3 – Terça, 17 de março

**Percepções de risco – o que pesa nas nossas percepções? Conceitos e perspectivas metodológicas**

#### Leitura obrigatória

* Di Giulio, G.M.; Vasconcellos, M. P.; Gunther, W. M. R.; Ribeiro, H.; Assunção, J. V. Percepção de risco: um campo de interesse para a interface Ambiente, Saúde e Sustentabilidade. 2015. Saúde e Sociedade, 24(4), 1217-1231. 
* Spink, M.J. 2014. Viver em áreas de risco: tensões entre gestão de desastres ambientais e os sentidos de risco no cotidiano. Ciência & Saúde Coletiva, 19(9):3743-3754, 2014.

#### Leitura complementar

* Renn, O. 2004. Perception of risks. Toxicology Letters, 149: 405–413.
* Furnival, A. C.; Pinheiro, S. M. 2008. A percepção pública da informação sobre os potenciais riscos dos transgênicos na cadeia alimentar. História, Ciências, Saúde – Manguinhos, Rio de Janeiro, v.15, n.2, p.277-291, abr.-jun. 2008.

### 24/03 – não haverá encontro presencial

### Semana 4 – Terça, 31 de março

**Interações entre ciência e política. Qual é o papel da ciência nas arenas socioambientais e arenas de riscos?**

#### Leitura obrigatória

* Viglio, J.E. et al. Narrativas científicas sobre petróleo e mudanças do clima e suas reverberações na política climática brasileira. Sociologias, Porto Alegre, ano 21, n. 51, maio-ago 2019, p. 124-159.
* Hannigan, J.A. 1995. Environmental sociology – a social construction perspective. Routledge, London (capítulo 7)
* Leite, J. C. 2015. Controvérsias na climatologia: o IPCC e o aquecimento global antropogênico. scientiæ studia, São Paulo, v. 13, n. 3, p. 643-77.

### Leitura complementar

* Jeronimo, H.M. 2014. Riscophrenia and "animal spirits": clarifying the notions of risk and uncertainty in environmental problems. Sci. stud. [online]. 2014, vol.12, n.spe [cited  2015-02-13], pp. 57-74.
* Renn, O. 1992. The Social Arena Concept of Risk Debates. In: S. Krimsky and D. Golding (eds.): Social Theories of Risk. Westport, CT (Praeger 1992), pp. 179-197.

### Semana 5 – Terça, 07 de abril

**Comunicação e suas possibilidades de intervenção no processo de construção social da realidade. Como a mídia cobre riscos e incertezas? Usabilidade do conhecimento científico: como integrar produtores e usuários da informação?**

#### Leitura obrigatória

* Hannigan, J.A. 2006. Environmental sociology – a social construction perspective. Routledge, London (capítulo 6)
* Rodas, C.A.; Di Giulio, G.M. Mídia brasileira e mudanças climáticas: uma análise sobre tendências da cobertura jornalística, abordagens e critérios de noticiabilidade. DESENVOLVIMENTO E MEIO AMBIENTE (UFPR), v. 40, p. 101-124, 2017.
* Lemos, M. C.; Kirchhoff, C. J.; Ramprasad, V. 2012. Narrowing the climate information usability gap. Nature Climate Change. 2, 789-94. doi: doi:10.1038/nclimate1614

#### Leitura complementar

* Lemos, M.C. et al. To co-produce or not to co-produce. Nature Sustainability. Vol. 1. Dec. 2018
* Kasperson, R. 2014. Four questions for risk communication. Journal of Risk Research, 17:10, 1233-1239, DOI: 10.1080/13669877.2014.900207.

### Semana 6 – Terça, 14 de abril

**Governança do risco. Transições para sustentabilidade na Era do Antropoceno. Para onde vamos?**

#### Leitura obrigatória

* Renn, O. 2008. Risk governance: coping with uncertainty in a complex world. Earthscan, London (capítulos 8 e 9).
* Bauman, Z. 2001. Modernidade líquida. Rio de Janeiro: Zahar (prefácio). 
* Guivant, J.S., Macnaghten, P. O mito do consenso: uma perspectiva comparativa sobre governança tecnológica. Ambient. soc. [online]. 2011, vol.14, n.2 [cited  2015-02-27], pp. 89-104
* Lahsen, M. Revelado pelo Google: transformação em direção à sustentabilidade requer repensar a ciência. ClimaCom. ANO 05 - N12 - "Dialogos do Antropoceno”

#### Leitura complementar

* Spruijt, P. et al. 2014. Roles of scientists as policy advisers on complex issues: A literature review. Environmental Science & policy 40 (2014) 16–25.

### Avaliação final / Fechamento – 05 de maio

## Dinâmica e avaliação das aulas


* Apresentação dos ensaios individuais
* Avaliação conjunta da disciplina*

## Participação nas aulas 
Enviar até a noite de segunda-feira para o e-mail da professora (ggiulio@usp.br) dois ou três parágrafos que exprimam reflexões sobre os textos indicados (pelo menos os obrigatórios) que serão discutidos na aula de terça-feira. Essas reflexões serão compiladas pela professora em um único documento, compartilhado com todos posteriormente, que servirá como base inicial para o debate. Para cada semana, esta atividade valerá 0,5 na média final (ao todo: 2,5).
 
## Debates e reflexões em grupo
Em cada aula, na última hora será trabalhado um texto curto relacionado à temática abordada para motivar reflexões sobre os conceitos discutidos e suas aplicações e pensar à luz das experiências vivenciadas ou estudadas pelos integrantes dos grupos.

## Proposta de trabalho final
Como trabalho final para a disciplina a proposta é que o estudante produza um ENSAIO (individual), de até 4 páginas, sobre tema pertinente a sua área de pesquisa ou atuação (a ser escolhido e desenvolvido por cada um) e que dialogue com o referencial teórico adotado e discutido ao longo das aulas. 
Espera-se um texto criativo, direto mas denso, preciso mas crítico. Como todo ensaio, ao final devem ser colocadas todas as referências bibliográficas citadas (fiquem à vontade para dialogar com outros autores, para além daqueles com os quais vamos trabalhar).
Formato: Times New Roman, tamanho 12, espaçamento de 1,5.
O trabalho final deve ser enviado para ggiulio@usp.br até 05/05 e valerá até 7,5. No encontro de avaliação final, os estudantes lerão seus ensaios para a turma que, conjuntamente, fará uma avaliação.

**Média final: atividades das semanas (até 2,5) + ensaio final (até 7,5)**