# Semana 2 – Terça, 10 de março

**A relação entre sociedade e ambiente. Riscos: incertezas fabricadas? O que se coloca como desafio à interface risco, ambiente e saúde global?**

## Leitura obrigatória

Beck, U. Nascimento, Sebastião (trad). Sociedade de risco rumo a uma outra modernidade.   2. ed..  São Paulo, Ed. 34, 2011. 383 p (prefácio e capítulo 1)

Giddens, A. Risk and Responsibility. The Modern Law Review Limited 1999 (MLR 62:1, January).

Fortes, P.A.C.; Ribeiro, H. Saúde Global em tempos de globalização. Saude soc. [online]. 2014, vol.23, n.2 [cited  2015-02-23], pp. 366-375.

## Leitura complementar

Bosco, E. Ulrich Beck: considerações sobre sua contribuição para os estudos em ambiente e sociedade e desafios. Ambiente & Sociedade, n. 2 n p. 149-160 n abr.-jun. 2015.

Hannigan, J.A. 2006. Environmental sociology – a social construction perspective. Routledge, London (capítulo 8)

Lieber, R.R. e Romano-Lieber, N.S. 2002. O conceito de risco: Janus reinventado. In: Minayo, M.C.S. e Miranda, A.C. (orgs). Saúde e ambiente: estreitando nós. Fiocruz, Rio de Janeiro, p. 69-111.

Guivant, J.S. 1998. A trajetória das análises de risco: da periferia ao centro da teoria social. Revista Brasileira de Informações Bibliográficas, Anpocs, 46: 3-38.